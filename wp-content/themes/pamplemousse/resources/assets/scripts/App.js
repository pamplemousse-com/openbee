import 'promise-polyfill/src/polyfill';
import Router from './util/Router'
import CheckCSSHelper from './util/CheckCSSHelper'
import Home from './routes/Home'
import PageTemplatePageContact from './routes/pageTemplatePageContact'
import bowser from 'bowser'

export default class App {
  constructor() {
    this.checkNavigator()
    this.router()
    this.checkCSS()
  }

  checkNavigator() {
    console.log('ready')
    console.log(bowser.name)
    if(bowser.name == 'Safari') document.body.classList.add('isSafari')
    if(bowser.name == 'Microsoft Edge' || bowser.name == 'Internet Explorer') document.body.classList.add('isIE')
  }

  /**
    * Populate Router instance with DOM routes
    *
    * @returns {void}
    */
  router() {
    this.routes = new Router({
      home: Home,
      pageTemplatePageContact: PageTemplatePageContact
    })
  }

  checkCSS() {
    const checkCSSHelper = new CheckCSSHelper()

    checkCSSHelper.load()
      .then(() => {
        this.routes.init()
      })
  }
}
