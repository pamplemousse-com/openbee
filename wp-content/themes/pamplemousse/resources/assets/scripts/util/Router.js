/* ========================================================================
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 * ======================================================================== */

import Page from '../routes/Page'
import camelCase from './camelCase'

// The routing fires all common scripts, followed by the page specific scripts.
export default class Router {
  constructor(routes) {
    this.canStartRoute = this.canStartRoute.bind(this)
    this.routes = routes
    this.currentRoute = null
    this.bodyClasses = []
  }

  // Fire page-specific JS if found
  init() {
    this.bodyClasses = this.getBodyClassesAsArray()

    const results = this.bodyClasses.filter(this.canStartRoute)

    if (results.length === 0) {
      this.currentRoute = new Page()

      return
    } else if (results.length > 1) console.warn('Multiple routes have been found, starting the first one...', results)

    this.startRoute(results)
  }

  getBodyClassesAsArray() {
    return document.body.className.toLowerCase()
      .replace(/-/g, '_')
      .split(/\s+/)
      .map(camelCase)
  }

  // Checks if the class is not empty,
  // if a given route has the same name
  // and extends the Page class
  canStartRoute(className) {
    if (className !== '' && this.routes[className]) {
      if (this.routes[className].prototype instanceof Page) return true

      console.warn('Your route "' + className + '" must extend the "Page" class')
    }

    return false
  }

  startRoute(route) {
    this.currentRoute = new this.routes[route[0]]
  }
}
