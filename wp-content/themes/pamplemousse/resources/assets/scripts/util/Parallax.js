import { TweenLite } from 'gsap'
import { debounce } from 'throttle-debounce'

export default class Parallax {

  /**
    * Initializes the parallax on this page
    *
    * @param {Number} inertia - (Default: `0.07`) The amount of inertia, bad results over 0.1
    * @param {String} selector - (Default: `.parallax-el`) The selector to get your parallaxed elems with
    * @param {Node} parent - (Default: `document`) The parent node in which retreive the parallaxed elems
    * @param {Number} disableUnder - (Default: `0`) If given, the parallax will deactivate itself when the window width is under `disableUnder` value
    * @param {Number} startDelay - (Default: `0`) Number of milliseconds to wait before initializing events
    */
  constructor({ inertia, selector, parent, disableUnder, startDelay } = {}) {
    this.scroll = this.scroll.bind(this)
    this.render = this.render.bind(this)
    this.resize = this.resize.bind(this)

    this.resizeDebounced = debounce(100, this.resize)

    this.SCROLL_INERTIA = inertia || 0.07
    this.scrollY = 0
    this.scrollInertiaY = 0
    this.lastScrollInertiaY = 0
    this.elems = []
    this.selector = selector || '.parallax-el'
    this.parent = parent || document
    this.disableUnder = disableUnder || 0

    this.init(startDelay)
  }

  init(startDelay = 0, parent = this.parent) {
    if (this.active) return

    this.active = true
    this.parent = parent

    const elements = this.parent.querySelectorAll(this.selector)

    if (elements.length === 0) return

    for (let i = 0; i < elements.length; i++) {
      const el = elements[i]

      if (!el.getAttribute('data-parallax-speed')) el.setAttribute('data-parallax-speed', (Math.random() * (0.03 - 0.01) + 0.01).toFixed(3))

      this.addParallaxEl(el, el.parentElement, el.getAttribute('data-parallax-speed'))
    }

    window.addEventListener('resize', this.resize)
    window.addEventListener('resize', this.resizeDebounced)

    if (window.innerWidth <= this.disableUnder) return

    window.addEventListener('scroll', this.scroll)

    TweenLite.ticker.addEventListener('tick', this.render)

    if (startDelay > 0) {
      setTimeout(() => {
        this.resize()
        this.animateParralax()
      }, startDelay)
    } else {
      this.resize()
      this.animateParralax()
    }
  }

  destroy() {
    if (!this.active) return

    window.removeEventListener('scroll', this.scroll)

    TweenLite.ticker.removeEventListener('tick', this.render)

    for (let i = 0; i < this.elems.length; i++) {
      const el = this.elems[i]

      TweenLite.set(el.el, {
        y: 0,
        force3D: true
      })
    }

    this.scrollY = 0
    this.scrollInertiaY = 0
    this.lastScrollInertiaY = 0
    this.elems = []

    this.active = false
  }

  reset(startDelay, parent) {
    this.destroy()
    this.init(startDelay, parent)
  }

  addParallaxEl(el, parentEl, speed = 0.02) {
    this.elems.push({
      el,
      parent: parentEl,
      speed
    })
  }

  setParallaxPos() {
    for (let i = 0; i < this.elems.length; i++) {
      const el = this.elems[i]
      const parentTopOffset = el.parent.getBoundingClientRect().top + this.scrollY

      el.posY = parentTopOffset + (el.parent.offsetHeight - window.innerHeight) / 2
    }
  }

  render() {
    if (this.scrollInertiaY === this.lastScrollInertiaY) return

    this.lastScrollInertiaY = this.scrollInertiaY

    this.animateParralax()
  }

  animateParralax() {
    for (let i = 0; i < this.elems.length; i++) {
      const el = this.elems[i]
      const posY = (this.scrollInertiaY - el.posY) * el.speed

      TweenLite.set(el.el, {
        y: -posY,
        force3D: true
      })
    }
  }

  scroll() {
    this.scrollY = window.scrollY || window.pageYOffset
    this.scrollInertiaY = this.getInertia(this.scrollY, this.scrollInertiaY, this.SCROLL_INERTIA)
  }

  getInertia(destinationValue, value, inertia) {
    const valueToAdd = Math.abs((destinationValue - value) * inertia) >= 0.01
      ? (destinationValue - value) * inertia
      : destinationValue - value
    let theValue = value

    theValue += valueToAdd

    return theValue
  }

  resize() {
    this.scroll()
    this.setParallaxPos()

    if (window.innerWidth <= this.disableUnder) this.destroy()
    else this.init()
  }
}
