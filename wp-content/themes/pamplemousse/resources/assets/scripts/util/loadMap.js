import GoogleMapsLoader from 'google-maps'

export default function(callback) {
  // eslint-disable-next-line no-undef
  GoogleMapsLoader.KEY = wp_vars.googleMapsApiKey
  GoogleMapsLoader.load(callback)
}
