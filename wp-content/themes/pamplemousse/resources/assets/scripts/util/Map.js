import GoogleMapsLoader from 'google-maps'

export default class Map {
	constructor({ styles, markerIcon, zoom } = {}) {
		this.zoom = zoom || 15
    // eslint-disable-next-line no-undef
		this.key = wp_vars.googleMapsApiKey
		this.initialized = false
		this.styles = styles
		this.markerIcon = markerIcon

		this.resize = this.resize.bind(this)
	}

	load() {
		return new Promise((resolve) => {
			GoogleMapsLoader.KEY = this.key

			GoogleMapsLoader.load((google) => {
				this.google = google

				resolve(this)
			})
		})
	}

	init() {
		this.initialized = true
		this.maps = []

		const mapElems = document.querySelectorAll('.acf-map')

		for (let i = 0; i < mapElems.length; i++) {
			this.maps.push(this.newMap(mapElems[i]))
		}

		window.addEventListener('resize', this.resize)
	}

	destroy() {
		if (!this.initialized) return

		window.removeEventListener('resize', this.resize)

		this.initialized = false
		this.maps = []
	}

	resize() {
		if (!this.initialized || this.maps.length === 0) return

		for (let i = 0; i < this.maps.length; i++) {
			this.centerMap(this.maps[i])
		}
	}

	newMap(el) {
		const markers = el.querySelectorAll('.marker')
		const map = new this.google.maps.Map(el, {
			zoom: this.zoom,
			center: new this.google.maps.LatLng(0, 0),
			mapTypeId: this.google.maps.MapTypeId.ROADMAP,
			styles: this.styles
		})

		map.markers = []
		map.infoWindows = []

		for (let i = 0; i < markers.length; i++) {
			this.addMarker(markers[i], map)
		}

		this.centerMap(map)

		return map
	}

	addMarker(marker, map) {
		const latlng = new this.google.maps.LatLng(marker.getAttribute('data-lat'), marker.getAttribute('data-lng'))
		const theMarker = new this.google.maps.Marker({
			position: latlng,
			icon: this.markerIcon,
			map
		})

		map.markers.push(theMarker)

		// if marker contains HTML, add it to an infoWindow
		if (marker.innerHTML) {
			const infowindow = new this.google.maps.InfoWindow({ content: marker.innerHTML })

			this.google.maps.event.addListener(theMarker, 'click', () => {
				for (let i = 0; i < map.infoWindows.length; i++) {
					map.infoWindows[i].close()
				}

				infowindow.open(map, theMarker)
			})

			map.infoWindows.push(infowindow)
		}
	}

	centerMap(map) {
		const bounds = new this.google.maps.LatLngBounds()

		for (let i = 0; i < map.markers.length; i++) {
			const latlng = new this.google.maps.LatLng(map.markers[i].position.lat(), map.markers[i].position.lng())

			bounds.extend(latlng)
		}

		if (map.markers.length === 1) {
			map.setCenter(bounds.getCenter())
			map.setZoom(this.zoom)
		} else map.fitBounds(bounds)
	}
}
