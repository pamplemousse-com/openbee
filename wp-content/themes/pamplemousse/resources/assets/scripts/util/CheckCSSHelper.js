/**
 * HOW TO USE :
 *
 * ```
 * const checkCSSHelper = new CheckCSSHelper()
 *
 * checkCSSHelper.load()
 *   .then(() => {
 *
 *   })
 * ```
 *
**/

export default class CheckCSSHelper {
  constructor() {
    this.raf = null
    this.checkCSS = this.checkCSS.bind(this)
  }

  load() {
    return new Promise((resolve) => {
      /* eslint no-process-env: "off" */
      if (process.env.NODE_ENV === 'production') document.addEventListener('DOMContentLoaded', resolve)
      else {
        this.loadDevMode()
          .then(resolve)
      }
    })
  }

  loadDevMode() {
    return new Promise((resolve) => {
      this.resolve = resolve

      this.checkCSS()
    })
  }

  checkCSS() {
    this.raf = requestAnimationFrame(this.checkCSS)

    // CSS loaded?
    if (getComputedStyle(document.body).boxSizing === 'border-box') {
      cancelAnimationFrame(this.raf)

      this.resolve()
    }
  }
}
