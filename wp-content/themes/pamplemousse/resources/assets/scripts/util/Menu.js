// import { TimelineLite } from 'gsap'

export default class Menu {
  constructor() {
    this.animating = false
    this.opened = false

    this.toggle = this.toggle.bind(this)
    this.open = this.open.bind(this)
    this.close = this.close.bind(this)

    this.getElems()
    this.events()
  }

  /**
    * Gets menu's DOM elements
    *
    * @returns {void}
    */
  getElems() {}

  /**
    * Launches events on DOM elements
    *
    * @returns {void}
    */
  events() {}

  /**
    * Animates the menu opening
    *
    * NOTE: Turn `this.animating` to `false` on anim END
    * NOTE: Turn `this.opened` to `true` on anim END
    *
    * @returns {void}
    */
  open() {
    if (this.opened || this.animating) return

    this.animating = true

    // const tl = new TimelineLite({
    //   onComplete: () => {
    //     this.animating = false
    //     this.opened = true
    //   }
    // })

    document.documentElement.classList.add('menu-opened')
  }

  /**
    * Animates the menu closing
    *
    * @param {event} e - The click event
    * @param {bool} force - Forces the menu to close
    *
    * NOTE: Turn `this.animating` to `false` on anim END
    * NOTE: Turn `this.opened` to `false` on anim END
    *
    * @returns {void}
    */
  close(e, force) {
    if ((!this.opened || this.animating) && !force) return

    this.animating = true

    // const tl = new TimelineLite({
    //   onComplete: () => {
    //     this.animating = false
    //     this.opened = false
    //   }
    // })

    document.documentElement.classList.remove('menu-opened')
  }

  /**
    * Toggles the menu opening/closing
    *
    * @returns {void}
    */
  toggle() {
    if (this.animating) return

    if (this.opened) this.close()
    else this.open()
  }

  /**
    * Called by App.js on `resize` event
    *
    * @returns {void}
    */
  resize() {}
}
