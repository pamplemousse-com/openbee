/*
  - `min` the bounds minimum value

  - `max` the bounds maximum value

  - `acceleration` is a range greater than 0 and <= 1.
    This is how responsive to change the chase value is.
    Low values will make the control seem sluggish or heavy.
    Higher values make the chase quick to respond and seem light to the touch.

  - `drag` is a value greater than 0 and <= 1
    it's more like a damping spring in function.
    Values less than 0.5 provide a smooth stop to the counting value.
    Values >= 0.5 cause the chase value to bounce
    around the required value, as the drag value moves
    towards 1 the bounce become more and more pronounced.

  - `reflect` describes the behaviour if the value crosses the bounds
    by defining the reflection or bounce.
    0 makes it stop dead at the bounds,
    < 0 and >= -1 will give a small bounce from the ends.
    Other values create interesting FX
*/

export default class Inertia {
  constructor(min, max, acceleration, drag, reflect) {
    this.ac = acceleration
    this.dr = drag
    this.minV = min
    this.maxV = max
    this.ref = -Math.abs(reflect)
    this.value = min
    this.delta = 0
  }

  // Call this function in a requestAnimationFrame
  // Returns the given value modified by inertia
  update(input) {
    // Once a frame, accelerate towards the input value by adding to deltaV
    this.delta += (input - this.value) * this.ac

    // Add drag to the delta by reducing its magnitude
    this.delta *= this.dr

    // Then add the deltaV to the chasing value
    this.value += this.delta

    // Bounds control
    if (this.value < this.minV) {
      this.value = this.minV

      if (this.delta < 0) this.delta *= this.ref
    } else if (this.value > this.maxV) {
      this.value = this.maxV

      if (this.delta > 0) this.delta *= this.ref
    }

    return this.value
  }

  // This moves the value to the required value without any inertial or drag
  // Is bound checked
  setValue(input) {
    this.delta = 0
    this.value = Math.min(this.maxV, Math.min(this.minV, input))

    return this.value
  }
}
