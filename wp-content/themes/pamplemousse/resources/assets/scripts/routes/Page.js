import Flickity from 'flickity-imagesloaded'
import 'objectFitPolyfill'
import { Power2, TimelineLite, TweenLite, Back } from 'gsap'
import SplitText from '../gsap/utils/SplitText.js'
import emergence from 'emergence.js'
import Rellax from 'rellax'
import Inertia from '../util/Inertia'
import Sticky from 'sticky-js'

Flickity.defaults.arrowShape = 'M70,90L10,50L70,10'

export default class Page {
  constructor() {
    this.render = this.render.bind(this)

    this.getElems()
    this.events()
    this.prepareEmergenceItems()
    this.initEmergence()
    this.loadParallax()
    this.render()
  }



  /* ----- GLOBAL ----- */
  getElems() {

    this.inertias = []

    this.leftValue = null
    this.rightValue = null
    this.topValue = null
    this.bottomValue = null

    this.maxDistance = 50
    this.mouse = {x: 0, y: 0}
    this.titles = document.querySelectorAll('h1, h2')
    this.itemsLogo = document.querySelectorAll('.approach__item__background')
    for(let i = 0 ; i<this.itemsLogo.length ; i ++) {
      this.inertias[i] = {
        left: new Inertia(-15, 15, 0.2, 0.75, 0.5),
        right: new Inertia(-15, 15, 0.2, 0.75, 0.5),
        top: new Inertia(-15, 15, 0.2, 0.75, 0.5),
        bottom: new Inertia(-15, 15, 0.2, 0.75, 0.5),
      }
    }

    const sticky = new Sticky('.sticky', {
      marginTop: 0
    })
  }
  events() {
    $(window).on('load resize', e => {
      const el = document.querySelector('.header__hamburger')
      const rect = el && el.getClientRects()[0]
      const center = rect && `${rect.left + rect.width/2}px ${rect.top + rect.height/2}px`
      const radius = Math.hypot && Math.hypot(window.innerWidth, window.innerHeight)
      if( !center ) return
      document.querySelector('.header__nav').style.setProperty('--center', center)
      document.querySelector('.header__nav').style.setProperty('--radius', radius+'px')
    })

    $('html').attr('data-ua', navigator.userAgent)

    document.querySelector('.header__hamburger').addEventListener('click', () => {
      document.body.classList.toggle('is-menu-open')
    })

    window.addEventListener('mousemove', this.onMouseMove.bind(this))
  }

  onMouseMove(e) {
    this.mouse.x = e.clientX
    this.mouse.y = e.clientY
  }



  /* ----- EMERGENCE ----- */
  prepareEmergenceItems() {
    // edit on _global.scss too
    for (let p = 0 ; p < this.titles.length ; p++) {
      this.titles[p].classList.add('animation-title')
      this.titles[p].dataset.emergence = "hidden"
    }

    const approachWrapper = document.querySelector('.approach')
    if(approachWrapper) {
      const approachItems = approachWrapper.querySelectorAll('.approach__item')
      const approachItemsLogos = approachWrapper.querySelectorAll('.approach__item__logo')
      TweenLite.set(approachItems, { y: '+=150'})
      TweenLite.set(approachItemsLogos, { y: '+=100'})
    }

    const textsclassic = document.querySelectorAll('.wp-block-columns p, .wp-block-button')
    for (let p = 0 ; p < textsclassic.length ; p++) {
      textsclassic[p].dataset.emergence = "hidden"
    }
  }
  initEmergence() {
      emergence.init({
        elemCushion: 0.25,
        offsetTop: 80,
        reset: false,
        callback: (element, state) => {
          if (state === 'visible' && !element.animated) {
            /* TITLES */
            if(element.classList.contains("animation-title")){
              element.animated = true
              this.playTitleAnimation(element)
            }
            if(element.classList.contains("approach")){
              element.animated = true
              window.innerWidth > 768 && this.playApproachAnimation(element)
            }
          }
        }
      })
    }

  /* ----- PARALLAX -----*/
  loadParallax() {
    if( document.querySelector('[data-rellax-speed]') ) {
      new Rellax('[data-rellax-speed]', {
        center: true
      })
    }
  }

  /* ----- ANIMATIONS ----- */
  playTitleAnimation(title){
    const tl = new TimelineLite()
    let titleChars = new SplitText(title, {type:'words,chars'})
    titleChars = titleChars.chars
    const from = {
      x: 10,
      y: 10
    }
    const to = {
      x: 0,
      y: 0
    }
    const i = {
      x: 13,
      y: 1
    }
    const i2 = {
      x: 5,
      y: -2
    }

    title.style.opacity = 1
    tl.staggerTo(titleChars, 0.5, {
      bezier:[{x: from.x, y: from.y }, { x: i.x, y: i.y },  { x: i2.x, y: i2.y }, { x: to.x, y: to.y }],
      opacity: 1,
      ease: Power2.easeOut,
    }, 0.02);
  }
  playApproachAnimation(element) {
      const approachItems = element.querySelectorAll('.approach__item')
      const approachItemsLogos = element.querySelectorAll('.approach__item__logo')
      const tl = new TimelineLite()

      tl.staggerTo(approachItems, 0.8, {
        opacity: 1,
        y: '-=150',
        ease: Power2.easeOut
      }, 0.1)
      .staggerTo(approachItemsLogos, 1, {
        opacity: 1,
        y: '-=100',
        ease: Power2.easeOut
      }, 0.1, '-=1')

    }

  render() {

    if(window.innerWidth <= 768) return

    for(let i = 0 ; i< this.itemsLogo.length ; i++) {

      const itemRect = this.itemsLogo[i].getBoundingClientRect()
      const topPosition = itemRect.top
      const bottomPosition = itemRect.top + itemRect.height
      const leftPosition = itemRect.left
      const rightPosition = itemRect.left + itemRect.width

      if( // TOP
        this.mouse.y < topPosition + 75 &&
        this.mouse.y > topPosition - this.maxDistance &&
        this.mouse.x > leftPosition - this.maxDistance / 4 &&
        this.mouse.x < rightPosition + this.maxDistance / 4) {
          const distance = -(this.mouse.y - topPosition)
          const value = 33 - (33 * distance / this.maxDistance)
          this.topValue = value/2 > 20 ? 20 : value/2
        } else {
          this.topValue = 0
        }


      if( // BOTTOM
        this.mouse.y > bottomPosition - 75 &&
        this.mouse.y < bottomPosition + this.maxDistance &&
        this.mouse.x > leftPosition - this.maxDistance / 4 &&
        this.mouse.x < rightPosition + this.maxDistance / 4
       ){
         const distance = this.mouse.y - bottomPosition
         const value = 33 - (33 * distance / this.maxDistance)
         this.bottomValue = value/2 > 20 ? 20 : value/2
        } else {
         this.bottomValue = 0
        }


      if( // LEFT
        this.mouse.x < leftPosition + 75 &&
        this.mouse.x > leftPosition - this.maxDistance &&
        this.mouse.y > topPosition - this.maxDistance / 4 &&
        this.mouse.y < bottomPosition+ this.maxDistance / 4) {
          const distance = -(this.mouse.x - leftPosition)
          const value = 33 - (33 * distance / this.maxDistance)
          this.leftValue = value/2 > 20 ? 20 : value/2
        } else {
          this.leftValue = 0
        }


      if( // RIGHT
        this.mouse.x > rightPosition - 75 &&
        this.mouse.x < rightPosition + this.maxDistance &&
        this.mouse.y > topPosition - this.maxDistance / 4 &&
        this.mouse.y < bottomPosition+ this.maxDistance / 4
        ){
          const distance = this.mouse.x - rightPosition
          const value = 33 - (33 * distance / this.maxDistance)
          this.rightValue = value/2 > 20 ? 20 : value/2
        } else {
          this.rightValue = 0
        }

        this.topValue = this.inertias[i].top.update(this.topValue)
        this.leftValue = this.inertias[i].left.update(this.leftValue)
        this.bottomValue = this.inertias[i].bottom.update(this.bottomValue)
        this.rightValue = this.inertias[i].right.update(this.rightValue)

        this.itemsLogo[i].style.borderTopLeftRadius = 33 + this.topValue + '%'
        this.itemsLogo[i].style.borderBottomRightRadius = 33 + this.bottomValue + '%'
        this.itemsLogo[i].style.borderBottomLeftRadius = 33 + this.leftValue + '%'
        this.itemsLogo[i].style.borderTopRightRadius = 33 + this.rightValue + '%'

      }

    requestAnimationFrame(this.render.bind(this))
  }

}
