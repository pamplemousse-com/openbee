import Page from './Page'
import loadMap from '../util/loadMap'

export default class Home extends Page {
  constructor() {
    super()

    /*global google places b:true*/
    loadMap((google) => {
      const $map = $('#map')
      const lat = parseFloat($map.data('lat'))
      const lng = parseFloat($map.data('lng'))
      const center = new google.maps.LatLng(lat,lng)
      const zoom = 12

      const map = new google.maps.Map($('#map')[0], {
        scrollwheel: false,
        fullscreenControl: false,
        mapTypeControl: false,
        streetViewControl: false,
        center,
        zoom,
      })

      const marker = new google.maps.Marker({
        map,
        position: {
          lat,
          lng,
        },
      })
    })
  }
}
