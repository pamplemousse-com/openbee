<a
  href="{{$url}}"
  class="btn btn-{{$class or ''}}"
  title="{{$title}}"
  target="{{$target or ''}}"
  {{!empty($target) && $target === '_blank' ? 'rel="noopener"' : ''}}>
  @if(!empty($icon))
    <i class="fa fa-{{$icon}}"></i>
  @elseif($title)
    {{$title}}
  @endif
</a>
