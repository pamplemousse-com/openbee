<time datetime="{{date_i18n('c', get_post_time())}}">
  {{sprintf(pll__('news.date'), date_i18n(get_option('date_format'), get_post_time()))}}
</time>
