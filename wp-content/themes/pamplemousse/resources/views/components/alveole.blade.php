<picture class="alveole">
  @if(!empty($slot))
    {!!$slot!!}
  @elseif($imageID)
    {!!  wp_get_attachment_image($imageID, array(600, 600)) !!}
  @endif
</picture>
