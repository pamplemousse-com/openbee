<!doctype html>
<html @php(language_attributes())>
  @include('partials.head')

  @php($bodyClasses = has_block('acf/cover') ? 'has-cover' : '')

  <body @php(body_class($bodyClasses))>
    @if(get_field('google_tag_manager', 'options'))
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id={{get_field('google_tag_manager', 'options')}}" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    @endif

    @php(do_action('get_header'))

    @include('partials.header')

    <main class="content">
      @yield('content')
    </main>

    @php(do_action('get_footer'))

    @include('partials.footer')

    @php(wp_footer())
  </body>
</html>
