{{--
  Title: Vidéo
--}}

<section class="{{$block['classes']}}" data-block="{{$block['id']}}">
  @include('components.alveole', ['imageID' => get_field('image')['ID']])

  @if(get_field('video'))
    <button>
      <svg viewBox="0 0 36.4 36.4" width="36" height="36">
        <path fill="currentColor" d="M28.4,17.7L13.7,9.2c-0.2-0.1-0.5-0.1-0.7,0s-0.4,0.4-0.4,0.6v17c0,0.3,0.1,0.5,0.4,0.6 c0.1,0.1,0.2,0.1,0.4,0.1c0.1,0,0.2,0,0.4-0.1l14.7-8.5c0.2-0.1,0.4-0.4,0.4-0.6C28.8,18,28.6,17.8,28.4,17.7z M14,25.6V11 l12.6,7.3L14,25.6z"/>
        <path fill="currentColor" d="M18.2,0C8.1,0,0,8.2,0,18.2s8.2,18.2,18.2,18.2s18.2-8.2,18.2-18.2S28.3,0,18.2,0z M18.2,35.1 c-9.3,0-16.8-7.6-16.8-16.8S9,1.5,18.2,1.5S35,9.1,35,18.3S27.5,35.1,18.2,35.1z"/>
      </svg>
    </button>
    {!!get_field('video')!!}

    <script>
      (function($) {
        var $block = $('[data-block="{{$block['id']}}"]')
        var $iframe = $block.find('.popup-iframe')[0]

        $('body').click(function(e) {
          $block.removeClass('is-active')
          $iframe.contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
        })

        $block.find('button').click(function(e) {
          $block.toggleClass('is-active')
          $iframe.contentWindow.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*');
          e.stopPropagation()
        })
      })(jQuery)
    </script>
  @endif
</section>
