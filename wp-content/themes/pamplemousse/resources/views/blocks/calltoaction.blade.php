{{--
  Title: Call to action
--}}

@if(get_field('calltoaction'))
  <section class="{{$block['classes']}}" data-block="{{$block['id']}}">
    <ul class="calltoaction__list">
      @while(have_rows('calltoaction')) @php(the_row())
        <li class="calltoaction__item">
          <a
            class="calltoaction__link"
            href="{{get_sub_field('link')['url']}}"
            title="{{get_sub_field('link')['title']}}"
            target="{{get_sub_field('link')['target'] or ''}}"
            {{!empty(get_sub_field('link')['target']) && get_sub_field('link')['target'] === '_blank' ? 'rel="noopener"' : ''}}>
            {!!get_sub_field('icon')!!}
            {{get_sub_field('link')['title']}}
          </a>
        </li>
      @endwhile
    </ul>
  </section>
@endif
