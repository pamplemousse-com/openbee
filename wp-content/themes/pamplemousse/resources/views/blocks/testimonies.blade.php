{{--
  Title: Témoignages
--}}

<section class="{{$block['classes']}}" data-block="{{$block['id']}}">
  <ul class="testimonies__list" data-flickity>
    @while(have_rows('testimonies')) @php(the_row())
      <li class="testimonies__item">
        <span class="testimonies__item__img">
          @include('components.alveole', ['imageID' => get_sub_field('image')['ID']])
        </span>
        <span class="testimonies__item__text">
          {!!get_sub_field('text')!!}
        </span>
      </li>
    @endwhile
  </ul>
</section>
