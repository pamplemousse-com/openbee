{{--
  Title: Offres d'emploi
--}}

@php($crop = get_field('background')['ID'])

<section class="{{$block['classes']}}" data-block="{{$block['id']}}" style="background-image: url({{resize($crop, [1200,1200], true)['src']}})">
  <div class="jobs__text">
    {!!get_field('text')!!}
  </div>

  {{-- Only requested items, all by default --}}
  @php($jobsIDs = get_field('jobs') ? array_map(function($job) { return $job['item']->ID; }, get_field('jobs')) : [])

  @php(query_posts(['post_type' => 'job', 'showposts' => -1, 'post__in' => $jobsIDs]))
  @if(have_posts())
    <ul class="jobs__list">
      @while(have_posts()) @php(the_post())
        <li class="jobs__item">
          <strong>{{the_title()}}</strong>
          @include('components.date')
          @foreach(['domain', 'place', 'type'] as $k)
            @if(get_field($k, get_the_ID()))
              <span class="{{$k}}">
                <i class="fa" aria-hidden="true"></i>
                {{get_field($k, get_the_ID())}}</span>
            @endif
          @endforeach
          <small>{{strip_tags(get_the_excerpt())}}</small>
          {{-- pll__('job.apply') Needed for translation --}}
          @include('components.btn', ['url' => get_permalink(), 'title' => pll__('job.apply'), 'class' => 'alternate'])
        </li>
      @endwhile
    </ul>
  @endif
  @php(wp_reset_query())
</section>
