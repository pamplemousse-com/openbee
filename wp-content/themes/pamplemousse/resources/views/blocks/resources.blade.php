{{--
  Title: Ressources
--}}

@php($tab = isset($_GET['tab']) ? $_GET['tab'] : 0)
@php($i = 0)

<section class="{{$block['classes']}}" data-block="{{$block['id']}}">
  @if(get_field('has_filters'))
    <nav class="resources__filters btn-group">
      <button class="resources__filter btn @if ($i == $tab) is-selected @endif" data-filter="all">
        {{pll__('filters.all')}}
      </button>
      @php($i++)
      @foreach( get_terms('group') as $term )
        <button class="resources__filter btn @if ($i == $tab) is-selected @endif" data-filter="{{$term->slug}}">
          {{$term->name}}
        </button>
        @php($i++)
      @endforeach
    </nav>
  @endif

  {{-- Only requested items, all by default --}}
  @php($resourcesIDs = get_field('resources') ? array_map(function($res) { return $res['item']->ID; }, get_field('resources')) : [])

  @php(query_posts(['post_type' => 'resource', 'showposts' => -1, 'post__in' => $resourcesIDs]))
  @if(have_posts())
    <ul class="resources__list">
      @while(have_posts()) @php(the_post())
        <li class="resources__item" data-group="all {{implode(' ', array_map(function($group) { return $group->slug; }, wp_get_post_terms(get_the_ID(), 'group')))}}">
          @if(get_post_thumbnail_id())
            <div class="resources__image">
            @php
              $terms = wp_get_post_terms(get_the_ID(), 'group');
              $id = count($terms) > 0 ? $terms[0]->term_id : -1;
            @endphp
            @if ($id == 16 || $id == 101)<div class="image-wrapper">@endif
            @php($crop = resize(get_post_thumbnail_id(), [null,400], false))
            <img src="{{$crop['src']}}" alt="{{the_title()}}" width="{{$crop['width']}}" height="{{$crop['height']}}">
            @php($domains = wp_get_post_terms(get_the_ID(), 'domain'))
            @if($domains)
              <span>{{$domains[0]->name}}</span>
            @endif
            @if ($id == 16 || $id == 101)</div>@endif
            </div>
          @endif
          <strong>{{the_title()}}</strong>
          <small>{{the_content()}}</small>
        </li>
      @endwhile
    </ul>
  @endif
  @php(wp_reset_query())

  @if(get_field('has_filters'))
    <script>
      (function($) {
        var $block = $('[data-block="{{$block['id']}}"]')
        var filter = null

        $block.find('.resources__filter').click(function(e) {
          filter = $(e.currentTarget).data('filter')
          $(e.currentTarget).addClass('is-selected').siblings().removeClass('is-selected')
          $block.find('.resources__item').removeClass('is-hidden').filter(function(i,el) {
            return !$(el).is('[data-group*='+filter+']')
          }).addClass('is-hidden')
        })

        $block.find('.resources__filter:nth-child({{ $tab + 1 }})').click()
      })(jQuery)
    </script>
  @endif
</section>
