{{--
  Title: Fonctionnalités
--}}

@if(have_rows('features'))
  <section class="{{$block['classes']}}" data-block="{{$block['id']}}">
    <ul class="features__list container">
      @while(have_rows('features')) @php(the_row())
        <li class="features__item">
          <span class="features__item__icon">
            @component('components.alveole')
              {!!get_sub_field('icon')!!}
            @endcomponent
          </span>
          <h3 class="features__item__title">{{get_sub_field('title')}}</h3>
          <hr class="wp-block-separator">
          <span class="features__item__text">{{get_sub_field('text')}}</span>
          @include('components.btn', get_sub_field('link'))
        </li>
      @endwhile
    </ul>
  </section>
@endif
