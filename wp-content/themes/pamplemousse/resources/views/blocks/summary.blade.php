{{--
  Title: Sommaire
--}}

@if(have_rows('links'))
  <section class="{{$block['classes']}} alignfull" data-block="{{$block['id']}}">
    <div class="sticky">
      <nav class="summary__list container">
        @while(have_rows('links')) @php(the_row())
          <a
            href="{{get_sub_field('link')['url']}}"
            class="summary__item"
            title="{{get_sub_field('link')['title']}}"
            target="{{get_sub_field('link')['target'] or ''}}"
            {{!empty(get_sub_field('link')['target']) && get_sub_field('link')['target'] === '_blank' ? 'rel="noopener"' : ''}}>
            {{get_sub_field('link')['title']}}
          </a>
        @endwhile
      </nav>
    </div>

    <script>
      (function($) {
        var $block = $('[data-block="{{$block['id']}}"]')
        var currentTarget = null
        var oldTarget = null

        $block.find('.summary__item').click(function(e) {
          e.preventDefault()
          var hash = $(e.currentTarget).attr('href')
          var top = $(hash).offset().top
          var offset = $block.height() * 2

          oldTarget = currentTarget
          currentTarget = e.currentTarget
          oldTarget && oldTarget.classList.remove('active')
          currentTarget && currentTarget.classList.add('active')

          $('html,body').animate({
            scrollTop: top - offset
          }, 350)
        })
      })(jQuery)
    </script>
  </section>
@endif
