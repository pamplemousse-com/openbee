{{--
  Title: Mis en avant
--}}

@php($crop = get_field('background')['ID'])
<section class="{{$block['classes']}}" data-block="{{$block['id']}}" style="background-image: url({{resize($crop, [1200,400], true)['src']}})">
  <div class="container wysiwyg">
    {!!get_field('text')!!}
  </div>
</section>
