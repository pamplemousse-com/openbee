{{--
  Title: Approche
--}}

<section class="{{$block['classes']}}" data-block="{{$block['id']}}" data-count="{{count(get_field('approaches'))}}" data-emergence="hidden" onClick="return true">
  <div class="container">
    <li class="approach__item approach__decoration">
      <span class="approach__item__background"></span>
    </li>
    <li class="approach__item approach__decoration">
      <span class="approach__item__background"></span>
    </li>
    <li class="approach__item approach__decoration">
      <span class="approach__item__background"></span>
    </li>
    <div class="approach__intro">
      {!!get_field('intro')!!}
    </div>
    <ul class="approach__list">
      @while(have_rows('approaches')) @php(the_row())
        <li class="approach__item">
          <span class="approach__item__background"></span>
          @if(get_sub_field('link-master'))
              <a class="approach__item__logo" href="{{ get_sub_field('link-master') }}">
          @else
              <span class="approach__item__logo">
          @endif
            <span class="approach__item__logo__background"></span>
            @if(get_sub_field('logo'))
              <img src="{{get_sub_field('logo')['url']}}" alt="{{get_sub_field('title')}}">
            @endif
            @if(get_sub_field('title'))
              <strong>{{get_sub_field('title')}}</strong>
            @endif

            @if(get_sub_field('link-master'))
                </a>
            @else
              </span>
            @endif

          <div class="approach__item__text">
            {!!get_sub_field('text')!!}
            @if(get_sub_field('links'))
              <nav class="approach__item__links" onClick="return true">
                <a class="approach__item__link">{{ pll_e('Selectionner') }}</a>
                @foreach(get_sub_field('links') as $link)
                  <a href="{{ get_permalink($link['link-page']->ID) }}" class="approach__item__link">
                    {{$link['link-page']->post_title}}
                  </a>
                @endforeach
              </nav>
            @endif
          </div>
        </li>
      @endwhile
    </ul>
  </div>
</section>
