{{--
  Title: Couverture
--}}

{{-- Thumbnail by default --}}
@php($crop = get_field('background') ? get_field('background')['ID'] : get_post_thumbnail_id())

{{-- Alternate style for front page. Can't use is_front_page() in Gutenberg --}}
@php($isFront = get_option('page_on_front') == get_the_ID())

<header class="{{$block['classes']}} {{$isFront ? 'cover-alternate' : ''}}" data-block="{{$block['id']}}">
  @if($crop)
    <picture class="cover__background ">
      <source srcset="{{resize($crop, [1200,800], true)['src']}}" media="(min-width: 520px)">
      <source srcset="{{resize($crop, [520,300], true)['src']}}" media="(min-width: 0px)">
      <img src="{{resize($crop, [1200,800], true)['src']}}" alt="{{the_title()}}" data-object-fit>
    </picture>
  @endif

  @if(get_field('video'))
    <button data-emergence="hidden">
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34.23 34.23" class="svg-play">
        <path d="M28.09,17.51,13.55,9.1a.84.84,0,0,0-.69,0,.81.81,0,0,0-.4.59V26.51a.54.54,0,0,0,.4.59c.1.1.2.1.4.1a.75.75,0,0,0,.4-.1l14.54-8.41a.81.81,0,0,0,.4-.59A1,1,0,0,0,28.09,17.51Z" transform="translate(-0.88 -0.98)" style="fill: none;stroke: currentColor;stroke-miterlimit: 10; stroke-dashoffset: 120;stroke-dasharray: 120;"/>
        <path d="M18,34.71A16.62,16.62,0,1,1,34.62,18.1,16.65,16.65,0,0,1,18,34.71Z" transform="translate(-0.88 -0.98)" style="fill: none;stroke: currentColor;stroke-miterlimit: 10; stroke-dashoffset: 120;stroke-dasharray: 120;"/>
      </svg>
    </button>

    {!!get_field('video')!!}

    <script>
      (function($) {
        var $block = $('[data-block="{{$block['id']}}"]')
        var $iframe = $block.find('.popup-iframe')[0]

        $('body').click(function(e) {
          $block.removeClass('is-active')
          $iframe.contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}', '*');
        })

        $block.find('button').click(function(e) {
          $block.toggleClass('is-active')
          $iframe.contentWindow.postMessage('{"event":"command","func":"' + 'playVideo' + '","args":""}', '*');
          e.stopPropagation()
        })
      })(jQuery)
    </script>
  @endif

  <div class="container">
    <div class="wysiwyg">
      <div class="cover__text">
        @if(get_field('backlink') && get_field('backlink')['url'])
          <a
            href="{{get_field('backlink')['url']}}"
            class="cover__backlink"
            title="{{get_field('backlink')['title']}}"
            target="{{get_field('backlink')['target'] or ''}}"
            {{!empty(get_field('backlink')['target']) && get_field('backlink')['target'] === '_blank' ? 'rel="noopener"' : ''}}>
            <svg width="24" height="24" viewBox="0 0 24 24"><path fill="currentColor" transform="translate(24,24) rotate(180)" d="M13.025 1l-2.847 2.828 6.176 6.176h-16.354v3.992h16.354l-6.176 6.176 2.847 2.828 10.975-11z"/></svg>
            {{get_field('backlink')['title'] ?: pll__('cover.backlink')}}
          </a>
        @endif

        {!!get_field('text') ?: '<h1>'.get_the_title().'</h1>'!!}

        @if(is_singular('job'))
          <p>
            @foreach(['domain', 'place', 'type'] as $k)
              @if(get_field($k, get_the_ID()))
                <span class="cover__text__icon cover__text__icon-{{$k}}">
                  <i class="fa" aria-hidden="true"></i>
                  {{get_field($k, get_the_ID())}}</span>
              @endif
            @endforeach
          </p>
        @endif
      </div>

      <div class="cover__logo">
        @if(get_field('logo'))
          @include('components.alveole', ['imageID' => get_field('logo')['ID']])
        @endif
      </div>
    </div>
  </div>
</header>
