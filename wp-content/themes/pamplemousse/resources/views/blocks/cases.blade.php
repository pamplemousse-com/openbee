{{--
  Title: Cas clients
--}}

{{-- Only requested items, all by default --}}
@php($casesIDs = get_field('cases') ? array_map(function($res) { return $res['item']->ID; }, get_field('cases')) : [])

@php(query_posts(['post_type' => 'case', 'showposts' => -1, 'post__in' => $casesIDs]))
@if(have_posts())
  <section class="{{$block['classes']}}" data-block="{{$block['id']}}">
    <nav class="cases__list">
      @while(have_posts()) @php(the_post())
        <a href="{{the_permalink()}}" class="cases__item" title="{{the_title()}}">
          @if(get_post_thumbnail_id())
            @php($crop = resize(get_post_thumbnail_id(), [200,100], false))
            <img src="{{$crop['src']}}" alt="{{get_sub_field('title')}}" width="{{$crop['width']}}" height="{{$crop['height']}}">
          @endif
          <strong>{{the_title()}}</strong>
          @php($domains = wp_get_post_terms(get_the_ID(), 'domain'))
          @if($domains)
            <span>{{$domains[0]->name}}</span>
          @endif
        </a>
      @endwhile
    </nav>
  </section>
@endif
@php(wp_reset_query())
