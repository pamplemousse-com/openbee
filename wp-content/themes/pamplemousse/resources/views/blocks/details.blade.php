{{--
  Title: Détails
--}}

@if(have_rows('details'))
  <section class="{{$block['classes']}}" data-block="{{$block['id']}}">
    <dl class="details__list container">
      @while(have_rows('details')) @php(the_row())
        <dt>
          <span class="details__icon">{!!get_sub_field('icon')!!}</span>
          <h3 class="details__title">{{get_sub_field('title')}}</h3>
        </dt>
        <dd>
          {{get_sub_field('text')}}
          @include('components.btn', get_sub_field('link'))
        </dd>
      @endwhile
    </dl>
  </section>
@endif
