{{--
  Title: Références
--}}

<section class="{{$block['classes']}}" data-block="{{$block['id']}}">
  {!!get_field('text')!!}
  <ul class="references__list">
    @foreach(get_field('logos') as $logo)
      <li class="references__item">
        @php($crop = resize($logo['ID'], [null,100], false))
        <img src="{{$crop['src']}}" alt="{{$logo['caption'] or $logo['title']}}" width="{{$crop['width']}}" height="{{$crop['height']}}">
      </li>
    @endforeach
  </ul>
  @include('components.btn', get_field('link'))
</section>
