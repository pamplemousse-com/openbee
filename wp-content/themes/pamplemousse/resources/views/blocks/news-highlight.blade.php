{{--
  Title: Article à la une
--}}

<section class="{{$block['classes']}}" data-block="{{$block['id']}}">
  @php(setup_postdata($GLOBALS['post'] =& get_field('news')[0]))
    @include('partials.newsItem', ['class' => 'highlight'])
  @php(wp_reset_postdata())
</section>
