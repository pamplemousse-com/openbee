{{--
  Title: Onglets
--}}

<section class="{{$block['classes']}}" data-block="{{$block['id']}}">
  <nav class="tabs__links">
    @php($i=1)
    @while(have_rows('tabs')) @php(the_row())
      <a href="#{{$block['id']}}_{{sanitize_title(get_sub_field('tab'))}}" class="tabs__link btn btn-alternate {{$i==1 ? 'is-selected' : ''}}">{{get_sub_field('tab')}}</a>
      @php($i++)
    @endwhile
  </nav>

  @php($i=1)
  @while(have_rows('tabs')) @php(the_row())
    <div id="{{$block['id']}}_{{sanitize_title(get_sub_field('tab'))}}" class="tabs__panel {{$i==1 ? 'is-selected' : ''}}">
      {!!get_sub_field('text')!!}
      {!!get_sub_field('html')!!}
    </div>
    @php($i++)
  @endwhile

  <script>
    (function($) {
      var $block = $('[data-block="{{$block['id']}}"]')
      $block.find('.tabs__link').click(function(e) {
        e.preventDefault()
        var $link = $(e.currentTarget)
        var i = $link.index()
        $block.find('.tabs__link').removeClass('is-selected').eq(i).addClass('is-selected')
        $block.find('.tabs__panel').removeClass('is-selected').eq(i).addClass('is-selected')
      })
    })(jQuery)
  </script>
</section>
