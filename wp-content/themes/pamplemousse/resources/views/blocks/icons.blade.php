{{--
  Title: Icônes
--}}

@if(have_rows('icons'))
  <ul class="{{$block['classes']}}" data-block="{{$block['id']}}">
    @while(have_rows('icons')) @php(the_row())
      <li>
        {!!get_sub_field('icon')!!}
        <strong>{{get_sub_field('text')}}</strong>
      </li>
    @endwhile
  </ul>
@endif
