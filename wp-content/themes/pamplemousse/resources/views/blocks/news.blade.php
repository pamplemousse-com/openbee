{{--
  Title: Articles
--}}

<section class="{{$block['classes']}}" data-block="{{$block['id']}}">
  @php
    // Parse URL
    $category = !empty($_GET['category']) ? $_GET['category'] : null;
    $page = !empty(get_query_var('page')) ? get_query_var('page') : 1;
    // Generic query
    $query = ['post_type' => 'post', 'posts_per_page' => get_option('posts_per_page'), 'paged' => $page];
    // Only requested items, all by default
    if(get_field('news')) {
      $query['post__in'] = array_map(function($res) { return $res['item']->ID; }, get_field('news'));
    }
    // Filter by category in URL
    if(!empty($category)) {
      $query['tax_query'] = [
        [
        'taxonomy' => 'category',
        'field' => 'slug',
        'terms' => $category
        ]
      ];
    }
  @endphp

  @if(get_field('has_filters'))
    <nav class="news__filters btn-group">
      <a href="{{the_permalink()}}" class="news__filter btn btn-alternate {{empty($category) ? 'is-selected' : ''}}" data-filter="all">
        {{pll__('filters.all')}}
      </a>
      @foreach(get_terms('category') as $term)
        {{-- Hide "Non classé" --}}
        @if($term->term_id != 1)
          <a href="{{the_permalink()}}?category={{$term->slug}}" class="news__filter btn btn-alternate {{$category == $term->slug ? 'is-selected' : ''}}" data-filter="{{$term->slug}}">
            {{$term->name}}
          </a>
        @endif
      @endforeach
    </nav>
  @endif

  @php(query_posts($query))
  @if(have_posts())
    <nav class="news__list container">
      @while(have_posts()) @php(the_post())
        @include('partials.newsItem')
      @endwhile
    </nav>

    @php
      global $wp_query;
      $ppp = get_option('posts_per_page');
      $count = $wp_query->post_count;
      $found = $wp_query->found_posts;
    @endphp
    @if($found > $count)
      <nav class="pagination">
        @for($i = 1; $found > ($i-1) * $ppp; $i++)
          <a class="pagination__link {{$i == $page ? 'is-current' : ''}}" href="?page={{$i}}{{!empty($category) ? '&category='.$category : ''}}">{{$i}}</a>
        @endfor
      </nav>
    @endif
  @endif
  @php(wp_reset_query())
</section>
