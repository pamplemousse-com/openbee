{{--
  Title: Offres
--}}

<section class="{{$block['classes']}}" data-block="{{$block['id']}}">
  @while(have_rows('offers')) @php(the_row())
    <div class="offers__item">
      <img src="{{get_sub_field('logo')['url']}}" alt="{{get_sub_field('logo')['caption'] or get_sub_field('logo')['title']}}">
      {!!get_sub_field('text')!!}
      @include('components.btn', get_sub_field('link'))
    </div>
  @endwhile
</section>
