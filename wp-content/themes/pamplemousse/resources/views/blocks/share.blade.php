{{--
  Title: Partage réseaux sociaux
--}}

@if(function_exists('ADDTOANY_SHARE_SAVE_KIT'))
  <section class="{{$block['classes']}} container">
    <div class="share__inner">
      <h3 class="share__title">{{get_field('title')}}</h3>
      @php(ADDTOANY_SHARE_SAVE_KIT(['buttons' => ['facebook','twitter','linkedin']]))
    </div>
  </section>
@endif
