{{--
  Title: Alvéole
--}}
<div class="alveole__wrapper">
  @include('components.alveole', ['imageID' => get_field('image')['ID']])
</div>
