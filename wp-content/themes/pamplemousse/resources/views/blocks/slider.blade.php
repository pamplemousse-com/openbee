{{--
  Title: Slider
--}}

@if(get_field('images'))
  <section class="{{$block['classes']}}" data-block="{{$block['id']}}">
    <ul class="slider__list">
      @foreach(get_field('images') as $img)
        <li class="slider__item {{$loop->first ? 'is-selected' : ''}}">
          @include('components.alveole', ['imageID' => $img['ID']])
        </li>
      @endforeach
    </ul>

    @if(count(get_field('images')) > 1)
      <button class="slider__btn slider__btn-prev" data-incr="-1">
        <svg width="24" height="24" viewBox="0 0 24 24"><path fill="currentColor" d="M3 12l18-12v24z" transform="translate(-3)"/></svg>
      </button>
      <button class="slider__btn slider__btn-next" data-incr="1">
        <svg width="24" height="24" viewBox="0 0 24 24"><path fill="currentColor" d="M21 12l-18 12v-24z" transform="translate(3)"/></svg>
      </button>
    @endif

    <script>
      (function($) {
        var $block = $('[data-block="{{$block['id']}}"]')
        $block.find('.slider__btn').click(function(e) {
          var $btn = $(e.currentTarget)
          var incr = parseInt($btn.data('incr'))
          var $selected = $block.find('.slider__item.is-selected')
          var i = $selected.is(':last-child') ? 0 : $selected.index() + incr

          $block.find('.slider__item')
          .removeClass('is-selected')
          .eq(i)
          .addClass('is-selected')
        })
      })(jQuery)
    </script>
  </section>
@endif
