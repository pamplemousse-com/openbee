{{--
  Title: Slider (texte)
--}}

<section class="{{$block['classes']}}" data-block="{{$block['id']}}">
  <ul class="slider-text__list" data-flickity>
    @foreach(get_field('texts') as $text)
      <li class="slider-text__item">
        {!!$text['text']!!}
      </li>
    @endforeach
  </ul>
</section>
