{{--
  Title: Écran divisé
--}}

<section class="{{$block['classes']}}" data-block="{{$block['id']}}">
  @while(have_rows('sections')) @php(the_row())
    <div class="split__item">
      {!!get_sub_field('text')!!}
    </div>
  @endwhile
</section>
