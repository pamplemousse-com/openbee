{{--
  Title: Carte
--}}

@if(get_field('markers'))
  @php
    $markers = [];
    foreach(get_field('markers') as $marker) {
      array_push($markers, [
        'address' => $marker['marker']['address'],
        'lat' => $marker['marker']['lat'],
        'lng' => $marker['marker']['lng'],
        'tooltip' => $marker['tooltip']
      ]);
    }
  @endphp

  <section class="{{$block['classes']}}" data-block="{{$block['id']}}" data-markers="{{json_encode($markers)}}">
    <div class="map__wrapper">
      <div class="gmap"></div>
    </div>
    <div class="container">
      <div class="map__text">
        {!!get_field('text')!!}
      </div>
    </div>
    <li class="approach__item approach__decoration">
      <span class="approach__item__background"></span>
    </li>

    <script src="https://maps.googleapis.com/maps/api/js?key={{ get_field('google_maps', 'options') }}&callback={{$block['id']}}initMap"async defer></script>
    <script>
      function {{$block['id']}}initMap() {
        (function($) {
          var $block = $('[data-block="{{$block['id']}}"]')
          var bounds = new google.maps.LatLngBounds()
          var markers = $block.data('markers')
          var tooltips = []
          var map = new google.maps.Map($block.find('.gmap')[0], {
            scrollwheel: false,
            fullscreenControl: false,
            mapTypeControl: false,
            streetViewControl: false,
            zoom: 12,
            styles: [
              {
                  'featureType': 'water','elementType': 'geometry',
                  'stylers': [{'color': '#c8c8c8'},{'lightness': 17}]},{'featureType': 'landscape','elementType': 'geometry',
                  'stylers': [{'color': '#dfdfdf'},{'lightness': 20}]},{'featureType': 'road.highway','elementType': 'geometry.fill',
                  'stylers': [{'color': '#ffffff'},{'lightness': 17}]},{'featureType': 'road.highway','elementType': 'geometry.stroke',
                  'stylers': [{'color': '#ffffff'},{'lightness': 29},{'weight': 0.2}]
              },{
                  'featureType': 'road.arterial','elementType': 'geometry',
                  'stylers': [{'color': '#ffffff'},{'lightness': 18}]},{'featureType': 'road.local','elementType': 'geometry',
                  'stylers': [{'color': '#ffffff'},{'lightness': 16}]},{'featureType': 'poi','elementType': 'geometry',
                  'stylers': [{'color': '#f5f5f5'},{'lightness': 21}]},{'featureType': 'poi.park','elementType': 'geometry',
                  'stylers': [{'color': '#dedede'},{'lightness': 21}]},{'elementType': 'labels.text.stroke',
                  'stylers': [{'visibility': 'on'},{'color': '#ffffff'},{'lightness': 16}]},{'elementType': 'labels.text.fill',
                  'stylers': [{'saturation': 36},{'color': '#333333'},{'lightness': 40}]
              },{
                  'elementType': 'labels.icon',
                  'stylers': [{'visibility': 'off'}]
              },{
                  'featureType': 'transit','elementType': 'geometry',
                  'stylers': [{'color': '#f2f2f2'},{'lightness': 19}]},{'featureType': 'administrative','elementType': 'geometry.fill',
                  'stylers': [{'color': '#fefefe'},{'lightness': 20}]},{'featureType': 'administrative','elementType': 'geometry.stroke',
                  'stylers': [{'color': '#fefefe'},{'lightness': 17},{'weight': 1.2}]
              }
            ],
          })

          $.each(markers, function(i, marker) {
            var lat = parseFloat(marker.lat)
            var lng = parseFloat(marker.lng)

            var marker = new google.maps.Marker({
              map: map,
              position: {lat: lat, lng: lng},
              icon: {
          			url: '/wp-content/themes/pamplemousse/resources/assets/images/pictoopenbee.png',
          			scaledSize: new google.maps.Size(28.5, 45),
		        },
              title: marker.tooltip || marker.address,
            })

            var tooltip = new google.maps.InfoWindow({
              content: marker.title
            })
            tooltips.push(tooltip)

            google.maps.event.addListener(marker, 'click', function() {
              tooltips.forEach(function(tooltip) {
                tooltip.close()
              })
              tooltip.open(map,marker)
            })

            bounds.extend({lat: lat, lng: lng})
          })

          setTimeout(function(e){
            map.fitBounds(bounds)
          }, 1500);


          google.maps.event.addListener(map, 'click', function() {
            tooltips.forEach(function(tooltip) {
              tooltip.close()
            })
          })
        })(jQuery);
      }
    </script>
  </section>
@endif
