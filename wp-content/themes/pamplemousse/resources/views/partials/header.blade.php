<header class="header contained">
  <a href="{{ pll_home_url() }}" class="header__logo">
    {!! file_get_contents(get_template_directory() . '/assets/images/logo.svg') !!}
  </a>
  @php $phonegAcf = pll_current_language() == 'en' ? 'phone_en' : 'phone'; @endphp
  @if(get_field($phonegAcf, 'options'))
    <a href="tel:{{get_field($phonegAcf, 'options')}}" class="header__phone">
      <svg width="24" height="24" viewBox="0 0 24 24">
        <path fill="currentColor" d="M20 22.621l-3.521-6.795c-.008.004-1.974.97-2.064 1.011-2.24 1.086-6.799-7.82-4.609-8.994l2.083-1.026-3.493-6.817-2.106 1.039c-7.202 3.755 4.233 25.982 11.6 22.615.121-.055 2.102-1.029 2.11-1.033z"/>
      </svg>
    </a>
  @endif

  <nav class="header__nav">
    <ul class="header__menu">
      @foreach(wp_get_nav_sorted_menu_items('primary_navigation') as $link)
        <li class="{{$link->children ? 'has-children' : ''}}">
          <a href="{{$link->url}}" class="{{implode(' ', $link->classes)}}" title="{{$link->title}}">
            {{$link->title}}
          </a>
          @if($link->children)
            <ul data-parent="{{$link->title}}">
              @foreach($link->children as $child)
                <li>
                  <a href="{{$child->url}}" class="{{implode(' ', $child->classes)}}" title="{{$child->title}}">
                    @if(get_field('icon', $child))
                      <img src="{{get_field('icon', $child)['url']}}" alt="{{$child->title}}">
                    @else
                      {{$child->title}}
                    @endif
                  </a>
                </li>
              @endforeach
            </ul>
          @endif
        </li>
      @endforeach
    </ul>

    @if(function_exists('pll_the_languages'))
      <ul class="header__langs">
        @php(pll_the_languages(['display_names_as' => 'slug']))
      </ul>
    @endif
  </nav>

  <button class="header__hamburger">
    <span class="open" style="top: 25%;"></span>
    <span class="open" style="top: 45%;"></span>
    <span class="open" style="top: 65%;"></span>
    <span class="close" style="width: 4px; height: 24px;"></span>
    <span class="close" style="width: 24px; height: 4px;"></span>
  </button>
</header>
