<footer class="footer contained">
  @php $footerAcf = pll_current_language() == 'en' ? 'footer_en' : 'footer'; @endphp
  @while(have_rows($footerAcf, 'options')) @php(the_row())
    <nav class="footer__nav">
      @php
        wp_nav_menu([
          'menu_class' => 'footer__menu',
          'theme_location' => 'footer_navigation',
          'container' => '',
        ]);
      @endphp
    </nav>

    <p class="footer__social">
      <span>{!!get_sub_field('social')!!}</span>

      @if(get_sub_field('facebook'))
        <a href="{{get_sub_field('facebook')}}" class="footer__social__link footer__social__link-facebook">
          <svg width="24" height="24" viewBox="0 0 24 24"><path d="M9 8h-3v4h3v12h5v-12h3.642l.358-4h-4v-1.667c0-.955.192-1.333 1.115-1.333h2.885v-5h-3.808c-3.596 0-5.192 1.583-5.192 4.615v3.385z"/></svg>
        </a>
      @endif

      @if(get_sub_field('linkedin'))
        <a href="{{get_sub_field('linkedin')}}" class="footer__social__link footer__social__link-linkedin">
          <svg width="24" height="24" viewBox="0 0 24 24"><path d="M4.98 3.5c0 1.381-1.11 2.5-2.48 2.5s-2.48-1.119-2.48-2.5c0-1.38 1.11-2.5 2.48-2.5s2.48 1.12 2.48 2.5zm.02 4.5h-5v16h5v-16zm7.982 0h-4.968v16h4.969v-8.399c0-4.67 6.029-5.052 6.029 0v8.399h4.988v-10.131c0-7.88-8.922-7.593-11.018-3.714v-2.155z"/></svg>
        </a>
      @endif
    </p>

    @if(have_rows('links'))
      <nav class="footer__links">
        @while(have_rows('links')) @php(the_row())
          @include('components.btn', get_sub_field('link'))
        @endwhile
      </nav>
    @endif

    <hr class="footer__sep">

    <div class="footer__mentions">
      {!!get_sub_field('mentions')!!}
    </div>

    @if(function_exists('pll_the_languages'))
      <ul class="footer__langs">
        @php(pll_the_languages(['display_names_as' => 'slug']))
      </ul>
    @endif
  @endwhile

  @php $floatingAcf = pll_current_language() == 'en' ? 'floating_en' : 'floating'; @endphp
  @if(get_field($floatingAcf, 'options'))
    <p class="footer__floating">
      @include('components.btn', array_merge(['class' => 'alternate', 'icon' => 'commenting-o'], get_field($floatingAcf, 'options')))
    </p>
  @endif
</footer>
