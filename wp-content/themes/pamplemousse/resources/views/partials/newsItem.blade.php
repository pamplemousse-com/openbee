<a href="{{get_permalink()}}" class="newsItem {{!empty($class) ? 'newsItem-'.$class : ''}}" data-group="all {{implode(' ', array_map(function($group) { return $group->slug; }, wp_get_post_terms(get_the_ID(), 'group')))}}">
  <picture>
    @if(get_post_thumbnail_id())
      @php($crop = resize(get_post_thumbnail_id(), [null,400], false))
      <img
        src="{{$crop['src']}}"
        alt="{{the_title()}}"
        width="{{$crop['width']}}"
        height="{{$crop['height']}}"
        data-object-fit>
    @endif
  </picture>
  @include('components.date')
  <strong>{{the_title()}}</strong>
  <span>{{strip_tags(get_the_excerpt())}}</span>
  <object>
    {{-- {{pll__('news.readmore')}} Needed for translation --}}
    @include('components.btn', ['title' => pll__('news.readmore'), 'url' => get_permalink()])
  </object>
</a>
