@extends('layouts.base')

@section('content')
  <div class="p404">
    <h1 class="p404__title">404</h1>
    <p class="p404__desc">{{ __('Page non trouvée', 'pamplemousse') }}</p>
    <p class="p404__btn">
      @include('components.btn', ['url' => get_bloginfo('url'), 'title' => __('Retour à l\'accueil', 'pamplemousse')])
    </p>
  </div>
@endsection
