<?php

/*
 * Hierarchical menu items
 * https://gist.github.com/alexbrombal/9a67af43257c4b9660dc
 */

// This is the inner recursive function - do not use directly
function _nav_get_children($items, $parentId, $depth) {
  $children = array();
  foreach($items as $id => $child) {
    if($child->menu_item_parent == $parentId) {
      $child->depth = $depth;
      $child->children = _nav_get_children($items, $child->ID, $depth + 1);
      $children[] = $child;
    }
  }
  return $children;
}
// Convenience wrapper to get a wordpress menu by location and sort it.
function wp_get_nav_sorted_menu_items($location) {
  return _nav_get_children(wp_get_nav_menu_items(get_nav_menu_locations()[$location]), 0, 0);
}

/*
 * Disable some default Gutenberg blocks
 * https://rudrastyh.com/gutenberg/remove-default-blocks.html
 */

add_filter( 'allowed_block_types', function( $allowed_blocks ) {
  $customBlocks = [];
  $dir = new DirectoryIterator(locate_template("views/blocks/"));
  // Loop through found blocks
  foreach ($dir as $fileinfo) {
    if (!$fileinfo->isDot()) {
      $slug = str_replace('.blade.php', '', $fileinfo->getFilename());
      $customBlocks[] = 'acf/'.$slug;
    }
  }
  return array_merge([
    'core/block',
    'core/paragraph',
    'core/heading',
    'core/list',
    'core/html',
    'core/button',
    'core/columns',
    'core/image',
    'core/separator',
  ], $customBlocks);
});

/*
 * Remove tags support from posts
 * https://stackoverflow.com/a/26260657/758728
 */

add_action('init', function() {
  unregister_taxonomy_for_object_type('post_tag', 'post');
});

/*
 * Don't load AddToAny styles
 */

add_action('wp_print_styles', function() {
  wp_dequeue_style('addtoany');
}, 100);

/*
 * Dequeue Gutenberg styles on front-end
 * https://github.com/WordPress/gutenberg/issues/7776#issuecomment-420700021
 */
add_action( 'wp_enqueue_scripts', function() {
  wp_dequeue_style( 'wp-block-library' );
}, 100 );

/*
 * Autoregister Gutenberg blocks with ACF
 */

function my_acf_block_render_callback( $block ) {
  $slug = str_replace('acf/', '', $block['name']);
  $block['slug'] = $slug;
  $block['classes'] = implode(' ', [$block['slug'], $block['className'], 'align'.$block['align']]);
  if ( is_admin() && function_exists( 'acf_maybe_get_POST' ) ) {
    if(!$GLOBALS['post'] && acf_maybe_get_POST('post_id')) {
      setup_postdata($GLOBALS['post'] =& get_post(acf_maybe_get_POST('post_id')));
    }
  }
  echo \App\template("blocks/${slug}", ['block' => $block]);
  wp_reset_postdata();
}

add_filter( 'block_categories', function( $categories, $post ) {
  return array_merge(
    $categories,
    [
      [
        'slug'  => sanitize_title(get_bloginfo('title')),
        'title' => get_bloginfo('title'),
      ]
    ]
  );
}, 10, 2 );

add_theme_support( 'align-wide' );

add_action('acf/init', function() {
  if( function_exists('acf_register_block') ) {
    // Look into views/blocks
    $dir = new DirectoryIterator(locate_template("views/blocks/"));
    // Loop through found blocks
    foreach ($dir as $fileinfo) {
      if (!$fileinfo->isDot()) {
        $slug = str_replace('.blade.php', '', $fileinfo->getFilename());
        // Get infos from file
        $file_path = locate_template("views/blocks/${slug}.blade.php");
        $file_headers = get_file_data($file_path, [
          'title' => 'Title',
          'description' => 'Description',
          'category' => 'Category',
          'icon' => 'Icon',
          'keywords' => 'Keywords',
        ]);
        if( empty($file_headers['title']) ) {
          die( _e('This block needs a title: ' . $file_path));
        }
        // Register a new block
        $datas = [
          'name' => $slug,
          'title' => $file_headers['title'],
          'description' => $file_headers['description'],
          'category' => sanitize_title(get_bloginfo('title')),
          'icon' => $file_headers['icon'],
          'keywords' => explode(' ', $file_headers['keywords']),
          'render_callback'  => 'my_acf_block_render_callback',
          'supports' => ['anchor' => true],
        ];
        acf_register_block($datas);
      }
    }
  }
});

/*
 * Trackduck
 */

add_action('wp_footer', function () {
  if( function_exists('acf_add_local_field_group') && get_field('trackduck', 'options')) {
    header('Access-Control-Allow-Origin: *.trackduck.com');
    echo '<script src="//cdn.trackduck.com/toolbar/prod/td.js" async data-trackduck-id="'.get_field('trackduck', 'options').'"></script>';
  }
});

// Polyfills
add_action('wp_enqueue_scripts', function () {
  // wp_enqueue_script('polyfill/picture', 'https://cdn.polyfill.io/v2/polyfill.js', [], null, true);
});

/*
 * GSAP Dev Tools
 */

// add_action('wp_enqueue_scripts', function () {
//   if( WP_DEBUG ) wp_enqueue_script('pamplemousse/GSDevTools', get_template_directory_uri().'/assets/scripts/gsap/utils/GSDevTools.min.js', ['sage/main.js'], null, true);
// }, 100);

/*
 * GTM option
 */

if( function_exists('acf_add_local_field_group') ):
  acf_add_local_field_group(array (
    'key' => 'group_58e34eb656ef2',
    'title' => 'Options',
    'fields' => array (
      array (
        'key' => 'field_592d67631463b',
        'label' => 'Code Google Tag Manager',
        'name' => 'google_tag_manager',
        'type' => 'text',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array (
          'width' => '',
          'class' => '',
          'id' => '',
        ),
        'default_value' => '',
        'placeholder' => 'GTM-XXXXXXX',
        'prepend' => '',
        'append' => '',
        'maxlength' => '',
      ),
      array (
        'key' => 'field_592d67631464b',
        'label' => 'Code Trackduck',
        'name' => 'trackduck',
        'type' => 'text',
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array (
          'width' => '',
          'class' => '',
          'id' => '',
        ),
        'default_value' => '',
        'placeholder' => 'XXXXXXXXXXXXX',
        'prepend' => '',
        'append' => '',
        'maxlength' => '',
      ),
        array (
            'key' => 'field_592d67693475b',
            'label' => 'Code Google Maps API',
            'name' => 'google_maps',
            'type' => 'text',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'default_value' => '',
            'placeholder' => 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX',
            'prepend' => '',
            'append' => '',
            'maxlength' => '',
        ),
    ),
    'location' => array (
      array (
        array (
          'param' => 'options_page',
          'operator' => '==',
          'value' => 'acf-options',
        ),
      ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
  ));
endif;

add_theme_support('post-thumbnails');

/**
 * Don't index preprod sites
 */
add_action( 'wp_head', function() {
  if(strpos($_SERVER['SERVER_NAME'], 'pamplemousse.site') !== false) {
    wp_no_robots();
  }
});

/**
 * Remove WordPress Default Image Sizes
 * https://wpbeaches.com/remove-wordpress-default-image-sizes/
 */

add_filter('max_srcset_image_width', create_function('', 'return 1;'));
add_filter('intermediate_image_sizes_advanced', function($sizes) {
  unset( $sizes['thumbnail']);
  unset( $sizes['medium']);
  unset( $sizes['medium-large']);
  unset( $sizes['large']);
  return $sizes;
});

/**
 * Remove Cookie consent styles
 * https://wordpress.org/support/topic/removing-cookie-notice-front-css/
 */

function dequeue_cookie_notice_front() {
  wp_dequeue_style('cookie-notice-front');
  wp_deregister_style('cookie-notice-front');
}
add_action( 'wp_enqueue_scripts', 'dequeue_cookie_notice_front', 9999 );
add_action( 'wp_head', 'dequeue_cookie_notice_front', 9999 );

/**
 * Add options page
 * https://www.advancedcustomfields.com/resources/options-page/
 */

if( function_exists('acf_add_options_page') ) {
  acf_add_options_page();
}

/**
 * Remove Emojis scripts & styles
 * https://korben.info/wordpress-comment-se-debarrasser-des-emojis.html
 */

remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

/**
 * Remove dashboard boxes loading AJAX contents
 * https://premium.wpmudev.org/forums/topic/remove-wordpress-news-widget-from-dashboard#post-1087866
 */

// Remove default dashboard news widget
function max_remove_dashboard_widgets() {
  remove_meta_box( 'dashboard_primary', 'dashboard', 'normal' );
}
add_action( 'admin_init', 'max_remove_dashboard_widgets' ); // this may work when you're trying to use wp_dashboard_setup and the dashboard_primary widget keeps showing up

// Remove network dashboard news widget
function max_remove_network_dashboard_widgets() {
  remove_meta_box('dashboard_primary', 'dashboard-network', 'side');
}
add_action( 'wp_network_dashboard_setup', 'max_remove_network_dashboard_widgets' );

/**
 * Don't load WPML assets
 * https://wpml.org/documentation/support/wpml-coding-api/
 */

define('ICL_DONT_LOAD_NAVIGATION_CSS', true);
define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);
define('ICL_DONT_LOAD_LANGUAGES_JS', true);


/**
 * Fix ACF Google Map API Key
 * https://www.advancedcustomfields.com/resources/acf-fields-google_map-api/
 */

add_filter('acf/fields/google_map/api', function($api) {
    $api['key'] = get_field('google_maps', 'options');
    return $api;
});

/**
 * Custom Admin Logo
 * https://codex.wordpress.org/Customizing_the_Login_Form
 */

function my_login_logo() { ?>
  <style type="text/css">
    #login h1 a, .login h1 a {
      background: url(<?php echo get_stylesheet_directory_uri(); ?>/resources/screenshot.svg) center no-repeat transparent/contain;
    }
  </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

// Reduces the excerpt length from 50 words to 20
function custom_excerpt_length($length) {
	return 20;
}
add_filter('excerpt_length', 'custom_excerpt_length', 999);

/**
 * Crop & resize images
 * https://github.com/junaidbhura/fly-dynamic-image-resizer/wiki
 */

function resize($attachment_id, $size, $crop) {
  if(function_exists('fly_get_attachment_image_src')) {
    return fly_get_attachment_image_src($attachment_id, $size, $crop);
  }
}

function wpb_mce_buttons_2($buttons) {
    array_unshift($buttons, 'styleselect');
    return $buttons;
}
add_filter('mce_buttons_2', 'wpb_mce_buttons_2');


function my_mce_before_init_insert_formats( $init_array ) {
    $style_formats = array(
        array(
            'title' => 'Sur-titre',
            'block' => 'span',
            'wrapper' => true,
        )
    );
    $init_array['style_formats'] = json_encode( $style_formats );
    return $init_array;
}
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );


// Adds support for categories and tags on pages
function add_categories_and_tags_for_pages() {
  register_taxonomy_for_object_type('post_tag', 'page');
  register_taxonomy_for_object_type('category', 'page');
}
add_action('init', 'add_categories_and_tags_for_pages');

add_action('init', function() {
  pll_register_string('Titre approache', 'Selectionner', 'pamplemousse');
});
